import { MenuLink } from '../abstracts/menu-link';

export const menuLinks: MenuLink[] = [
    {
        name: 'Главная',
        link: '/'
    },
    {
        name: 'Зарождение',
        link: '/origin'
    },
    {
        name: 'Конференции',
        link: '/perspective-conference'
    },
    {
        name: 'CTF',
        link: '/ctf'
    },
    {
        name: 'Направления',
        link: '/directions'
    }
];