import { OverviewBlock } from '../abstracts/overview-block';

export const overviewBlocks = [
    {
        imgPath: 'images/is-conference.jpg',
        imgAltText: 'Конференция “Информационная безопасность”',
        buttonText: 'Подробнее',
        title: 'Конференция “Информационная безопасность”', 
        link: '/is-conference',
        isSwap: true
    },
    {
        imgPath: 'images/perspective-conference.jpg',
        imgAltText: 'Конференция “Перспектива”',
        buttonText: 'Подробнее',
        title: 'Конференция “Перспектива”', 
        link: '/perspective-conference',
        isSwap: false
    },
    {
        imgPath: 'images/ctf.jpg',
        imgAltText: 'ctf',
        buttonText: 'Подробнее',
        title: 'CTF', 
        link: '/ctf',
        isSwap: false
    },
    {
        imgPath: 'images/sciencebit.jpg',
        imgAltText: 'Научная деятельность',
        buttonText: 'Подробнее',
        title: 'Научная деятельность', 
        link: '/science',
        isSwap: true
    }
];