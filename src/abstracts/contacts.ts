export interface Contact {
    name: string,
    email: string,
    icon: string,
    contact: string,
}