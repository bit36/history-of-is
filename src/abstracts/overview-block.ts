export interface OverviewBlock {
    blockData: {
        imgPath?: string,
        imgAltText?: string,
        buttonText: string,
        title: string, 
        link: string,
        isSwap: boolean
    }
}