export const achievements = [
  {
    image: 'images/firstVector.svg',
    description: '6-е место в финале PHDays IV'
  },
  {
    image: 'images/secondVector.svg',
    description: 'Russian Cybersecurity Competition - 3 место'
  },
  {
    image: 'images/thirdVector.svg',
    description: '2-е место на HackIM'
  },
  {
    image: 'images/fourthVector.svg',
    description: '7-е место на PHD CTF (3-е  среди российских команд)'
  },
  {
    image: 'images/fifthVector.svg',
    description: '4-е место на RuCTF Quals'
  },
  {
    image: 'images/sixthVector.svg',
    description: 'финал RuCTF (3-е место среди студенческих команд)'
  },
  {
    image: 'images/seventhVector.svg',
    description: '4-е место на ForbiddenBITS CTF'
  },
  {
    image: 'images/eightVector.svg',
    description: '2-е место Хакатон CyberGarden (ИБ проект)'
  },
];

export const placesOfWork = [
  'Вооруженные силы РФ',
  'Федеральная служба безопасности',
  'НИИ МВС ЮФУ',
  'ООО Спецстройсвязь, г. Таганрог',
  'ООО ТАГМЕТ, г. Таганрог',
  'ОАО Концерн «Вега», г. Москва',
  'ОАО «ИнфоТеКС», г. Москва',
  'Parallels, Inc.',
  'Intel Corp.',
  'Avira GmbH',
  'ЗАО Российские наукоёмкие технологии',
  'ЗАО Инфосистемы Джет, г. Москва',
  'IBS Dunice',
  'Oggetto'
]
