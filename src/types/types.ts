export interface IAchievement {
  image: string,
  description: string,
}

export interface PropPlaceOfWork {
  description: string,
}
